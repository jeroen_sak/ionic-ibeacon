import { Component, OnInit } from '@angular/core';
import { __core_private_testing_placeholder__ } from '@angular/core/testing';
import { BeaconService } from './beacon.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  public logging: string[] = [];
  public eventsLog: string[] = [];

  public iBeacons = [
    {
      name: 'Blueberry',
      id: 'B9407F30-F5F8-466E-AFF9-25556B57FE6D',
      major: 14197,
      minor: 45080,
      inRange: false,
      beaconService: null,
    },
    {
      name: 'Coconut',
      id: '5E6058EE-6BC8-4BE3-8A0F-73A72BF3B0FB',
      major: 63694,
      minor: 64006,
      enabled: false,
      beaconService: null,
    },
    {
      name: 'Ice',
      id: 'B9407F30-F5F8-466E-AFF9-25556B57FE6D',
      major: 29805,
      minor: 40992,
      enabled: false,
      beaconService: null,
    },
    {
      name: 'Mint',
      id: 'CBE8DF0F-1141-0105-47F1-38E042FC6FFB',
      major: 49062,
      minor: 35352,
      enabled: false,
      beaconService: null,
    },
  ];

  onEvent(name: string, event: string) {
    this.eventsLog.push(`${event}: ${name}`);
    const target = this.iBeacons.find(a => a.name === name);

    if (event === 'didEnterRegion') {
      target.inRange = true;
    } else if (event === 'didExitRegion') {
      target.inRange = false;
    }
  }

  onLog(message: string) {
    this.logging.push(`${JSON.stringify(message).replace(/\\/g, '')}`);
  }

  ngOnInit() {
    this.logging.push('lets start:');
    this.iBeacons.map(
      a =>
        (a.beaconService = new BeaconService(
          a.id,
          a.name,
          a.major,
          a.minor,
          {
            didEnterRegion: name => this.onEvent(name, 'didEnterRegion'),
            didExitRegion: name => this.onEvent(name, 'didExitRegion'),
            didStartMonitoringForRegion: name => this.onEvent(name, 'didStartMonitoringForRegion'),
          },
          (message: string) => this.onLog(message)
        ))
    );
  }
}
