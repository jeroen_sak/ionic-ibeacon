import { IBeacon } from '@ionic-native/ibeacon/ngx';

interface IBeaconEvents {
  didEnterRegion: Function;
  didExitRegion: Function;
  didStartMonitoringForRegion: Function;
}

export class BeaconService {
  private ibeacon: IBeacon;

  constructor(
    private uuid: string,
    private name: string,
    private major: number,
    private minor: number,
    private events: IBeaconEvents,
    private log: Function
  ) {
    this.ibeacon = new IBeacon();

    setTimeout(() => {
      this.init();
    }, 2000);
  }

  private init() {
    this.log('init');
    // create a new delegate and register it with the native layer
    this.log(`cordova: ${cordova ? 'yes' : 'no'}`);
    this.log(`plugins: ${cordova.plugins ? 'yes' : 'no'}`);
    // @ts-ignore
    this.log(`locationManager: ${cordova.plugins.locationManager ? 'yes' : 'no'}`);
    // @ts-ignore
    const delegate = new cordova.plugins.locationManager.Delegate();

    this.log(`delegate: ${delegate ? JSON.stringify(delegate.prototype) : 'no'}`);

    try {
      delegate.didStartMonitoringForRegion = (data: any) => {
        this.log(`didStartMonitoringForRegion: ${JSON.stringify(data)}`);
        this.events.didStartMonitoringForRegion(this.name);
      };
      delegate.didRangeBeaconsInRegion = (data: any) => {
        this.log(`didRangeBeaconsInRegion: ${JSON.stringify(data)}`);
      };
      delegate.didEnterRegion = (data: any) => {
        this.log(`didEnterRegion: ${JSON.stringify(data)}`);
        this.events.didEnterRegion(this.name);
      };
      delegate.didExitRegion = (data: any) => {
        this.log(`didExitRegion: ${JSON.stringify(data)}`);
        this.events.didExitRegion(this.name);
      };
      delegate.didDetermineStateForRegion = (data: any) => {
        this.log(`didDetermineStateForRegion: ${JSON.stringify(data)}`);
      };
      delegate.monitoringDidFailForRegionWithError = (data: any) => {
        this.log(`monitoringDidFailForRegionWithError: ${JSON.stringify(data)}`);
      };
      delegate.peripheralManagerDidUpdateState = (data: any) => {
        this.log(`peripheralManagerDidUpdateState: ${JSON.stringify(data)}`);
      };
      delegate.didChangeAuthorizationStatus = (data: any) => {
        this.log(`didChangeAuthorizationStatus: ${JSON.stringify(data)}`);
      };

      this.ibeacon.setDelegate(delegate);

      // @ts-ignore
      const beaconRegion = new cordova.plugins.locationManager.BeaconRegion(
        this.name,
        this.uuid,
        // this.major,
        // this.minor
      );

      this.ibeacon
        .startMonitoringForRegion(beaconRegion)
        .then(
          () => this.log('started startMonitoringForRegion'),
          error => this.log(`error startMonitoringForRegion: ${JSON.stringify(error)}`)
        );
    } catch (e) {
      this.log(`catch: ${e}`);
    }
  }
}
